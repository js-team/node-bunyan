Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: bunyan
Upstream-Contact: https://github.com/trentm/node-bunyan/issues
Source: https://github.com/trentm/node-bunyan#readme

Files: *
Copyright: 2009-2017, Trent Mick <trentm@gmail.com>
 2012-2017, Joyent Inc.
License: Expat

Files: exeunt/*
Copyright: 2017 Joyent, Inc.
License: MPL-2.0

Files: mv/*
Copyright: 2014, Andrew Kelley
License: Expat

Files: safejsonstringify/*
Copyright: 2014-2017, Debitoor <https://debitoor.com/>
License: Expat

Files: tools/jsstyle
Copyright: 2008, Sun Microsystems, Inc.
 2011, Joyent, Inc.
License: CDDL

Files: debian/*
Copyright: 2019-2021, Yadd <yadd@debian.org>
License: Expat

License: CDDL
 The contents of this file are subject to the terms of the
 Common Development and Distribution License (the "License").
 You may not use this file except in compliance with the License.
 .
 You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
 or https://www.opensolaris.org/os/licensing.
 See the License for the specific language governing permissions
 and limitations under the License.
 .
 When distributing Covered Code, include this CDDL HEADER in each
 file and include the License file at usr/src/OPENSOLARIS.LICENSE.
 If applicable, add the following below this CDDL HEADER, with the
 fields enclosed by brackets "[]" replaced with your own identifying
 information: Portions Copyright [yyyy] [name of copyright owner]

License: Expat
 Permission is hereby granted, free of charge, to any person
 obtaining a copy of this software and associated documentation files
 (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge,
 publish, distribute, sublicense, and/or sell copies of the Software,
 and to permit persons to whom the Software is furnished to do so,
 subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be
 included in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.

License: MPL-2.0
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Mozilla Public License version 2.
 .
 On Debian systems, the complete text of version 2 of the Mozilla
 Public License can be found in `/usr/share/common-licenses/MPL-2.0'
